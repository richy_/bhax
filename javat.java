public class Gagyi{
    public static void main(String[] args) {
        //nem vegtelen ciklus
         //Integer x = -128;
         //Integer t = -128;
         //Integer x = 127;
         //Integer t = 127;
        //vegtelen ciklus
         Integer x = 127;
         Integer t = 127;
         //Integer x = -129;
         //Integer t = -129;
         // Integer x = new Integer(127);
         // Integer t = new Integer(127);
         while (x <= t && x >= t && t != x) {
              System.out.println("while");
         }
    }
}